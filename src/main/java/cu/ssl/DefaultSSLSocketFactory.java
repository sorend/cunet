/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ssl;


import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * This class handles the creation of all ssl-related sockets.
 * To use the VIA PADLOCK acceleration, download and install the via padlock JCP here: http://www.via.com.tw/en/initiatives/padlock/via-jcp.jsp
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-11 : 01:16:19
 * @version $Id: DefaultSSLSocketFactory.java 262 2008-10-30 21:29:48Z jevring $
 */
public class DefaultSSLSocketFactory {
    private SSLServerSocketFactory ssf;
    private SSLSocketFactory csf;
    private static DefaultSSLSocketFactory factory;

    // NOTE: this is the SocketFactory that is used for encrypted SSL transfers.
    // We have to create our own socket factory, because the default ones can't wrap a currently existing socket in an SSL socket. bummer.
    public DefaultSSLSocketFactory(String keystoreLocation, String keystorePassphrase) throws NoSuchAlgorithmException, KeyStoreException, IOException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        SSLContext ctx = SSLContext.getInstance("TLSv1.2");
        // KeyStore:
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        KeyStore ks = KeyStore.getInstance("JKS");

        // keytool -genkey -alias cuftpd -keypass cuftpdpw -keystore cuftpd.keystore -storepass cuftpdpw -keyalg RSA -keysize 2048
        char[] passphrase = keystorePassphrase.toCharArray();
        // NOTE: ks.load uses the "-storepass" setting of the keytool

        final FileInputStream inputStream = new FileInputStream(keystoreLocation);
        ks.load(inputStream, passphrase);
        // NOTE: the kmd.init uses the "-keypass" setting of the keytool
        kmf.init(ks, passphrase);
        // TrustStore (TrustManager):
        TrustManager[] tm = {
            new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] x509Certificates, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] x509Certificates, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            }
        };

        // Initialize (Mr Zulu, ENGAGE!)
        SecureRandom sr = new SecureRandom();
        sr.nextInt(); // Initialize it here, where the overhead won't be noticed, instead of at the first socket creation
        ctx.init(kmf.getKeyManagers(), tm, sr);
        ssf = ctx.getServerSocketFactory();
        csf = ctx.getSocketFactory();
        inputStream.close();
    }

    public static DefaultSSLSocketFactory getFactory() {
        return factory;
    }

    // CLIENT SOCKETS
    /**
     * Wraps a normal socket in an SSLSocket.
     *
     * @param socket the socket to wrap.
     * @param useClientMode true if we are the client, false if we are the server.
     * @return the wrapped SSLSocket.
     * @throws java.io.IOException if some i/o error occurs
     */
    public SSLSocket wrapSocket(Socket socket, boolean useClientMode) throws IOException {
        SSLSocket upgradedSocket = (SSLSocket) csf.createSocket(socket, socket.getInetAddress().getHostAddress(), socket.getPort(), true);
        // this will be true when we're upgrading a socket we've opened. it will be false when we're upgrading a socket that resulted from a ServerSocket.accept().
        upgradedSocket.setUseClientMode(useClientMode);
        return upgradedSocket;
    }

    public SSLSocket createSocket(String hostname, int port, int timeout, boolean useClientMode) throws IOException {
        Socket s = new Socket();
        s.connect(new InetSocketAddress(hostname, port), timeout);

        SSLSocket socket = (SSLSocket) csf.createSocket(s, hostname, port, true);
        socket.setUseClientMode(useClientMode);
        return socket;
    }

    public SSLSocket createSocket(boolean useClientMode) throws IOException {
        SSLSocket s = (SSLSocket) csf.createSocket();
        s.setUseClientMode(useClientMode);
        return s;
    }

    public SSLSocket createSocket(String hostname, int port) throws IOException {
        SSLSocket s = (SSLSocket)csf.createSocket(hostname, port);
        s.setUseClientMode(true);
        return s; 
    }

    // SERVER SOCKETS
    public SSLServerSocket createServerSocket(int port, int backlog, InetAddress localAddress) throws IOException {
        return (SSLServerSocket) ssf.createServerSocket(port, backlog, localAddress);
    }

    public SSLServerSocket createServerSocket(int port) throws IOException {
        return (SSLServerSocket) ssf.createServerSocket(port);
    }

    public SSLServerSocket createServerSocket() throws IOException {
        return (SSLServerSocket) ssf.createServerSocket();
    }

    public static void createFactory(String keystoreLocation, String keystorePassword) throws IOException, NoSuchAlgorithmException, KeyManagementException, KeyStoreException, CertificateException, UnrecoverableKeyException {
        factory = new DefaultSSLSocketFactory(keystoreLocation, keystorePassword);
    }

    public static void checkKeystorePassword(String keystoreLocation, String keystorePassword) throws NoSuchAlgorithmException, KeyStoreException, IOException, CertificateException, UnrecoverableKeyException {
        KeyStore ks = KeyStore.getInstance("JKS");

        // NOTE: ks.load uses the "-storepass" setting of the keytool
        final FileInputStream inputStream = new FileInputStream(keystoreLocation);
        ks.load(inputStream, keystorePassword.toCharArray());
        inputStream.close();
        // ks.load() should throw an exception if the passphrase is wrong
    }
}
